package com.example.collections

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.example.collections.BBDD.Chapas
import com.example.collections.BBDD.DataBase
import kotlinx.android.synthetic.main.activity_mostrar_chapas.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MostrarChapasActivity : AppCompatActivity() {

    // creacion de la base de datos
    lateinit var db: DataBase

    // creamos variable globar
    lateinit var chapa: Chapas

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mostrar_chapas)

        // instanciamos la BBDD
        db = DataBase.getInstance(applicationContext)!!

        // recogemos la chapa a recuperar del intent
        val idChapa = intent.getStringExtra("chapa")
        val nombreCava = intent.getStringExtra("cava")

        // mostramos por pantalla el resultado de recuperar el intent
        mostrarChapa(idChapa.toInt(), nombreCava)
    }

    // sobreescribimos método para cerrar la activity en el back
    override fun onBackPressed() {
        finish()
    }

    // botón back pulsado, volvemos cerramos la activity
    fun rollback(view: View) {
        finish()
    }

    // mostramos la información en pantalla
    private fun mostrarChapa(idChapa: Int, nombreCava: String) {

        GlobalScope.launch {
            // recuperamos el id de la cava
            chapa = db.cavaDao().infoChapa(idChapa, nombreCava)
        }

        Handler().postDelayed(Runnable {
            // introducimos los valores en los correspondientes Views
            tv_descripcion.text = chapa.descripcion.toString()
            tv_detalles.text = chapa.motivo.toString()
            val imagen = Uri.parse(chapa.foto_asociada)
            iv_fotoChapa.setImageURI(imagen)
        }, 10)
    }
}
