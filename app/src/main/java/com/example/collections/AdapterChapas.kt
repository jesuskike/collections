package com.example.collections

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.collections.BBDD.Chapas
import kotlinx.android.synthetic.main.lista_chapas.view.*


class AdapterChapas (val items: ArrayList<Chapas>, val context: Context): RecyclerView.Adapter<AdapterChapas.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.lista_chapas, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        // iondicamos la descripción a mostrar
        val item = items[position]
        holder.tvItem.setText(item.descripcion.toString())

        // asignamos el id de la chapa al linear lyout y
        holder.llItem.setTag(item.id_chapa.toString())
        holder.btItem.setContentDescription(item.id_chapa.toString())

        // insertamos la foto en el recicleview
        val imagen = Uri.parse(item.foto_asociada)
        holder.ivItem.setImageURI(imagen)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvItem = view.tv_nombreChapa
        val ivItem = view.iv_foto
        val llItem = view.mostrarChapa
        val btItem = view.bt_borrarChapa
    }
}


