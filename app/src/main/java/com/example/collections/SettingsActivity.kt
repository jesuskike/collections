package com.example.collections

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_anyadir.bt_return
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_settings.*
import java.util.*


class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        //bloqueamos el giro de pantalla
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    // sobreescribimos método para cerrar la activity en el back
    override fun onBackPressed() {
        finish()
    }

    // botón back pulsado, retornamos a la activity main
    fun back(view: View) {
        finish()
    }

    // radio button castellano pulsado
    fun castellano(view: View) {
        cargarIdioma("es", "ES")
    }

    // radio button català pulsado
    fun catala(view: View) {
        cargarIdioma("ca", "ES")
    }

    // radio button english pulsado
    fun english(view: View) {
        cargarIdioma("en", "EN")
    }

    // función de cambio de idioma
    fun cargarIdioma(lengua: String, pais: String) {
        val localizacion = Locale(lengua, pais)
        Locale.setDefault(localizacion)
        val config = Configuration()
        config.locale = localizacion
        baseContext.resources
            .updateConfiguration(config, baseContext.resources.displayMetrics)

        // reseteamos el layout para hacer efectivo el cambio de idioma
        setContentView(R.layout.activity_settings)
    }
}
