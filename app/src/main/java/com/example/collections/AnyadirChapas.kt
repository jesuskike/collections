package com.example.collections

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Matrix
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.Surface
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import android.widget.Toast
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.example.collections.BBDD.Chapas
import com.example.collections.BBDD.DataBase
import com.example.collections.alerta.Alerta
import kotlinx.android.synthetic.main.activity_anyadir_chapas.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.util.concurrent.Executors


// This is an arbitrary number we are using to keep track of the permission
// request. Where an app has multiple context for requesting permission,
// this can help differentiate the different contexts.
private const val REQUEST_CODE_PERMISSIONS = 10

// This is an array of all the permission specified in the manifest.
private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)

class AnyadirChapas : AppCompatActivity(), LifecycleOwner {

    // creacion de la base de datos
    lateinit var db: DataBase

    // declaramos la variable que recoge el intent
    var nombreCava = ""

    // declaramos la variable que contiene la imagen de la chapa
    var imagenChapa = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anyadir_chapas)

        // instanciamos la BBDD
        db = DataBase.getInstance(applicationContext)!!

        // recogemos el dato del intent
        nombreCava = intent.getStringExtra("nombreCava")

        viewFinder = findViewById(R.id.tv_photoChapa)

        // Request camera permissions
        if (allPermissionsGranted()) {
            viewFinder.post { startCamera() }
        } else {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }

        // Every time the provided texture view changes, recompute layout
        viewFinder.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }
    }

    // añadimos la chapa a la BBDD
    fun addChapas(view: View) {
        // recogemos los datos de los campos
        val descripcion = et_descripcion.text.toString()
        val motivo = et_motivo.text.toString()

        // comprobamos que se haya insertado algo en los campos
        if (descripcion != "" && motivo != "") {

            // comprobamos que la foto esté presente
            if (imagenChapa != "" ) {

                // recuperamos el id de la cava
                GlobalScope.launch {
                    val cava_id = db.cavaDao().recuperaID(nombreCava)

                    // construimos el objeto para la BBDD
                    val temporal = Chapas(null, descripcion, imagenChapa, motivo, cava_id)

                    // ingresamos los datos en la BBDD
                    db.cavaDao().insertaChapa(temporal)
                }

                // informamos que se ha insertado el item
                Toast.makeText(this, R.string.info, Toast.LENGTH_LONG).show()

                // ponemos a 0 todos los items
                et_descripcion.setText("")
                et_motivo.setText("")
                imagenChapa = ""

                // cerramos el teclado
                val inputMethodManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

                inputMethodManager.hideSoftInputFromWindow(bt_anyadirChapas.getWindowToken(), 0)
            } else {
                mostrarAlerta(getString(R.string.noFoto), getString(R.string.Error))
            }
        } else {

            // mostramos la alerta de campo vacío
            mostrarAlerta(getString(R.string.camposVacios), getString(R.string.Error))
        }
    }

    // función para mostar una alerta de colección ya existente
    fun mostrarAlerta(mensaje: String, titulo: String) {
        val fragmentManager = supportFragmentManager
        val dialogo = Alerta(mensaje, titulo)
        dialogo.show(fragmentManager, "tagAlerta")
    }

    // sobreescribimos método para cerrar la activity en el back
    override fun onBackPressed() {
        devolverIntent()
    }

    // botón back pulsado, volvemos cerramos la activity
    fun rollback(view: View) {
        devolverIntent()
    }

    // devolvemos las colecciones añadidas a la main activity
    fun devolverIntent() {
        // comprobamos que no hay ninguna foto en memoria
        if (imagenChapa != "") {
            borrarFoto()
        }
        val returnIntent = Intent()
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    // función para borrar la foto si no se graba
    fun borrarFoto() {
        // recuperamos la dirección del fichero
        val fichero = File(imagenChapa)

        // borramos la foto
        fichero.delete()
    }

// Add this after onCreate

    private val executor = Executors.newSingleThreadExecutor()
    private lateinit var viewFinder: TextureView

    private fun startCamera() {

        // Create configuration object for the viewfinder use case
        val previewConfig = PreviewConfig.Builder().apply {
            setTargetResolution(Size(500, 500))
        }.build()


        // Build the viewfinder use case
        val preview = Preview(previewConfig)

        // Every time the viewfinder is updated, recompute layout
        preview.setOnPreviewOutputUpdateListener {

            // To update the SurfaceTexture, we have to remove it and re-add it
            val parent = viewFinder.parent as ViewGroup
            parent.removeView(viewFinder)
            parent.addView(viewFinder, 0)

            viewFinder.surfaceTexture = it.surfaceTexture
            updateTransform()
        }

        // Add this before CameraX.bindToLifecycle

        // Create configuration object for the image capture use case
        val imageCaptureConfig = ImageCaptureConfig.Builder()
            .apply {
                // We don't set a resolution for image capture; instead, we
                // select a capture mode which will infer the appropriate
                // resolution based on aspect ration and requested mode
                setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                setFlashMode(FlashMode.AUTO)
            }.build()

        // Build the image capture use case and attach button click listener
        val imageCapture = ImageCapture(imageCaptureConfig)
        findViewById<ImageButton>(R.id.bt_foto).setOnClickListener {
            val file = File(externalMediaDirs.first(),
                "${System.currentTimeMillis()}.jpg")

            // comprobamos que nos se haya realizado una foto previamente
            if (imagenChapa != ""){
                borrarFoto()
            }

            // recogemos el nombre del fichero para guardarlo en la bbdd
            imagenChapa = file.toString()

            imageCapture.takePicture(file, executor,
                object : ImageCapture.OnImageSavedListener {
                    override fun onError(
                        imageCaptureError: ImageCapture.ImageCaptureError,
                        message: String,
                        exc: Throwable?
                    ) {
                        val msg = "Photo capture failed: $message"
                        Log.e("CameraXApp", msg, exc)
                        viewFinder.post {
                            Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onImageSaved(file: File) {
                        val msg = "Photo capture succeeded: ${file.absolutePath}"
                        Log.d("CameraXApp", msg)
                        viewFinder.post {
                            Toast.makeText(baseContext, R.string.photoCaptured, Toast.LENGTH_SHORT).show()
                        }
                    }
                })
        }

        // Add this before CameraX.bindToLifecycle

        // Setup image analysis pipeline that computes average pixel luminance
        val analyzerConfig = ImageAnalysisConfig.Builder().apply {
            // In our analysis, we care more about the latest image than
            // analyzing *every* image
            setImageReaderMode(
                ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
        }.build()

        // Build the image analysis use case and instantiate our analyzer
        val analyzerUseCase = ImageAnalysis(analyzerConfig).apply {
            setAnalyzer(executor, LuminosityAnalyzer())
        }

        // Bind use cases to lifecycle
        // If Android Studio complains about "this" being not a LifecycleOwner
        // try rebuilding the project or updating the appcompat dependency to
        // version 1.1.0 or higher.
        CameraX.bindToLifecycle(this, preview, imageCapture, analyzerUseCase)
    }

    private fun updateTransform() {
        val matrix = Matrix()

        // Compute the center of the view finder
        val centerX = viewFinder.width / 2f
        val centerY = viewFinder.height / 2f

        // Correct preview output to account for display rotation
        val rotationDegrees = when(viewFinder.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)

        // Finally, apply transformations to our TextureView
        viewFinder.setTransform(matrix)
    }

    /**
     * Process result from permission request dialog box, has the request
     * been granted? If yes, start Camera. Otherwise display a toast
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                viewFinder.post { startCamera() }
            } else {
                Toast.makeText(this,
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it) == PackageManager.PERMISSION_GRANTED
    }
}


