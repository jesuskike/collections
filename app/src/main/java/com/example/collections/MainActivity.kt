package com.example.collections

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo.*
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.collections.BBDD.Cava
import com.example.collections.BBDD.DataBase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.lista_chapas.view.*
import kotlinx.android.synthetic.main.lista_items.*
import kotlinx.android.synthetic.main.lista_items.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    // creacion de la base de datos
    lateinit var db: DataBase

    // codigo de respuesta del intent
    val REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // instanciamos la BBDD
        db = DataBase.getInstance(applicationContext)!!

        // cargamos los datos
        addItems()
        configView()

        // escuchamos los botones
        buttonListening()
    }

    // declaramos un array para mostrar las colecciones
    private var lista: ArrayList<String> = ArrayList()

    // sobreescribimos método para anular el botón de atrás del dispositivo
    override fun onBackPressed() {
        finish()
    }

    // escuchamos los botones pulsados
    private fun buttonListening() {

        // boton para las opciones
        bt_options.setOnClickListener {
            val intent = Intent(applicationContext, SettingsActivity::class.java)
            startActivity(intent)
        }

        // botón de búsqueda pulsado
        bt_search.setOnClickListener {
            val intent = Intent(applicationContext, SearchActivity::class.java)
            startActivity(intent)
        }

        // botón de añadir pulsado
        bt_anyadir.setOnClickListener {
            startActivityForResult(
                Intent(this, AnyadirActivity::class.java).apply
                { putExtra("lista", lista) }, REQUEST_CODE)
        }
    }

    // recogemos los nombres de la lista del Recycle View
    fun mostrarLista(view: View) {

        // recogemos el texto asignado al botón que será el nombre de la colección
        val coleccion = view.bt_lista.getText()

        // activamos la actividad de cámara
        val intent = Intent(applicationContext, ColeccionActivity::class.java)

        // enviamos el nombre de la colección para la búsqueda en la BBDD
        intent.putExtra("nombreColeccion", coleccion.toString())
        startActivity(intent)
    }

    // borramos la colección seleccionada
    fun borrarColeccion(view: View){

        // verificamos que queremos borrar la colección y mostramos dialogo de confirmación
        val alertbox = AlertDialog.Builder(this)

        // asignamos la cadena a mostrar
        alertbox.setMessage(R.string.confirmacionCava)

        // pulsamos sobre si
        alertbox.setPositiveButton( R.string.yes,
            DialogInterface.OnClickListener { arg0, arg1 ->

                GlobalScope.launch {
                    // recuperamos el indice de la colección
                    val idColeccion = view.bt_borrarColeccion.getContentDescription().toString()

                    // recuperamos las chapas para borrarlas
                    val chapas = db.cavaDao().loadChapas(idColeccion)

                    for (item in chapas) {

                        // borramos la foto del dispositivo
                        val fichero = File(item.foto_asociada)
                        fichero.delete()

                        // borramos la chapa de la BBDD
                        db.cavaDao().borrarChapa(item)
                    }

                    // recuperamos el objeto de la colecció
                    val coleccion = db.cavaDao().existeCava(idColeccion)

                    // borramos la coleccion de la BBDD
                    db.cavaDao().borrarColeccion(coleccion)

                    // borramos la colección de la lista
                    lista.remove(idColeccion)
                }

                Handler().postDelayed(Runnable {
                    // recargamos la vista
                    configView()
                }, 1)

            })

        // pulsamos sobre NO
        alertbox.setNegativeButton( R.string.no,
            DialogInterface.OnClickListener { arg0, arg1 ->

                //ha pulsado no y cancelamos el borrrado
                arg0.cancel()
            })

        //mostramos el alertbox
        alertbox.show()
    }

    // lectura de las cavas de la BBDD
    private fun addItems() {
        GlobalScope.launch {
            // borramos la lista
            lista.clear()

            // cargamos la lista de la BBDD
            val items = db.cavaDao().loadCavas()

            // recorremos la lista obtenida para insertarla en la Recicle View
            for (nombre in items) {
                lista.add(nombre.nombreCava.toString())
            }
        }
    }

    // configuración de la vista de la Recicle View
    private fun configView() {
        rv_lista.adapter = AdapterMain(lista, this)
        rv_lista.layoutManager = LinearLayoutManager( this)
    }

    // recargamos la actividad después de volver de la anterior
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // comprobamos la recepción de los datos
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            // cargamos los datos
            addItems()
            configView()
        }
    }

}
