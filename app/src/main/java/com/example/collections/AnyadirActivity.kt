package com.example.collections


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.collections.BBDD.Cava
import com.example.collections.BBDD.DataBase
import com.example.collections.alerta.Alerta
import kotlinx.android.synthetic.main.activity_anyadir.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList


class AnyadirActivity : AppCompatActivity() {

    // creacion de la base de datos
    lateinit var db: DataBase

    // recogemos la lista de colecciones
    var lista: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anyadir)

        // instanciamos la BBDD
        db = DataBase.getInstance(applicationContext)!!

        // recogemos los valores de la lista
        lista = intent.getStringArrayListExtra("lista")

        // escuchamos los botones
        buttonListener()
    }

    // sobreescribimos método para cerrar la activity en el back
    override fun onBackPressed() {
        devolverIntent()
    }

    // escucha de botones
    private fun buttonListener() {

        // botón de añadir colección pulsado
        bt_sutmit.setOnClickListener {
            add()
        }

        // botón de volver pulsado
        bt_return.setOnClickListener {
            devolverIntent()
        }
    }

    // botón de añadir colección pulsado
    fun add() {

        // variable que indica si existe o no la colección en la lista
        var existe = false

        // recogemos el contenido del campo introducido
        val valor = et_nombreColeccion.text.toString().trim().toUpperCase(Locale.getDefault())

        // comprobamos que el campo introducido tenga algún valor
        if (valor != "") {

            // recorremos la lista buscando la colección
            for (contenido in lista) {
                if (contenido == valor) {
                    existe = true
                }
            }

            // comprobamos que el nombre no esté en la lista
            if (existe) {
                // mostramos alerta de que la colección ya existe
                mostrarAlerta(getString(R.string.mostrarAlerta), getString(R.string.Alerta))

            } else {
                // añadimos la colección a la lista
                lista.add(valor)

                // actualizamos la BBDD
                GlobalScope.launch {
                    // montamos la clase a grabar
                    val temporal = Cava(null, valor)

                    // grabamos el dato en la BBDD
                    db.cavaDao().insert(temporal)
                }

                // indicamos que se ha grabado el dato
                Toast.makeText(this, getText(R.string.grabado), Toast.LENGTH_LONG).show()

                // reseteamos el text edit
                et_nombreColeccion.setText("")

                // cerramos el teclado
                val inputMethodManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

                inputMethodManager.hideSoftInputFromWindow(bt_sutmit.getWindowToken(), 0)

            }
        } else {

            // mostramos la alerta de campo vacío
            mostrarAlerta(getString(R.string.mostrarError), getString(R.string.Error))
        }
    }

    // función para mostar una alerta de colección ya existente
    fun mostrarAlerta(mensaje: String, titulo: String) {
        val fragmentManager = supportFragmentManager
        val dialogo = Alerta(mensaje, titulo)
        dialogo.show(fragmentManager, "tagAlerta")
    }

    // devolvemos las colecciones añadidas a la main activity
    fun devolverIntent() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }
}
