package com.example.collections.alerta

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.example.collections.R

class Alerta (mensaje: String, titulo: String) : DialogFragment() {

    private val mensaje = mensaje
    private val titulo = titulo

    // sobreescribimos la función de creación del box de alerta
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        // realizamos el buider de la activity
        val builder = AlertDialog.Builder(activity)

        // montamos el titulo y el mensaje de la alerta
        builder.setMessage(mensaje).setTitle(titulo).setPositiveButton("OK")
        { dialog, id -> dialog.cancel() }

        // retornamos la creación del builder
        return builder.create()
    }
}