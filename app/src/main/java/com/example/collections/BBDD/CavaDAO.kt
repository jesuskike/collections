package com.example.collections.BBDD

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface CavaDAO {

    // insertamos el nombre de la cava
    @Insert(onConflict = REPLACE)
    fun insert(cava: Cava)

    // recuperamos todas las cavas para el recicle view
    @Query("SELECT * FROM cavas")
    fun loadCavas(): Array<Cava>

    // comprobamos si existe la cava
    @Query("SELECT * FROM cavas WHERE nombre_cava LIKE :nombre")
    fun existeCava(nombre: String): Cava

    // recuperamos el ID de la cava
    @Query("SELECT id_cava FROM cavas WHERE nombre_cava LIKE :nombre")
    fun recuperaID(nombre: String): Int

    // recuperamos las chapas de una cava
    @Query("SELECT * FROM chapas as ch, cavas as ca WHERE ca.nombre_cava LIKE :nombreCava AND ca.id_cava = ch.cava_id")
    fun loadChapas(nombreCava: String): Array<Chapas>

    // recuperamos la información de una chapa
    @Query("SELECT * FROM chapas AS ch, cavas AS ca WHERE ca.nombre_cava LIKE :nombreCava AND ca.id_cava = ch.cava_id AND ch.id_chapa = :idChapa")
    fun infoChapa(idChapa: Int, nombreCava: String): Chapas

    // insertamos las chapas en una cava específica
    @Insert(onConflict = REPLACE)
    fun insertaChapa(chapa: Chapas)

    // borrado de la chapa de la BBDD
    @Delete
    fun borrarChapa(vararg chapa: Chapas)

    // borrado de coleccion
    @Delete
    fun borrarColeccion(vararg cava: Cava)
}