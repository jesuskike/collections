package com.example.collections.BBDD

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity (tableName = "cavas")
data class Cava (
    @PrimaryKey(autoGenerate = true) var id_cava: Int?,
    @ColumnInfo(name = "nombre_cava") var nombreCava: String?
)

