package com.example.collections.BBDD

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [Cava::class, Chapas::class],
    version = 1
)


abstract class DataBase : RoomDatabase() {

    abstract fun cavaDao(): CavaDAO

    companion object {

        private var INSTANCE: DataBase? = null

        fun getInstance(context: Context): DataBase? {

            if (INSTANCE == null){
                synchronized(DataBase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        DataBase::class.java, "coleccion.db").build()
                }
            }

            return INSTANCE
        }

        fun destroyInstance(){
            INSTANCE = null
        }
    }
}
