package com.example.collections.BBDD

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.io.File

@Entity (tableName = "chapas",
    foreignKeys = arrayOf(
        ForeignKey(
            entity = Cava::class,
            parentColumns = arrayOf("id_cava"),
            childColumns = arrayOf("cava_id"))
    ))

data class Chapas (
    @PrimaryKey(autoGenerate = true) var id_chapa: Int?,
    var descripcion: String?,
    var foto_asociada: String?,
    var motivo: String?,
    @ColumnInfo(name = "cava_id") var cava_id: Int
)