package com.example.collections

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.lista_items.view.*

class AdapterMain (val items: ArrayList<String>, val context: Context): RecyclerView.Adapter<AdapterMain.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.lista_items, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.btItem.setText(item)
        holder.btborrar.setContentDescription(item)
    }

    class ViewHolder (view: View): RecyclerView.ViewHolder(view){
        val btItem = view.bt_lista
        val btborrar = view.bt_borrarColeccion
    }
}