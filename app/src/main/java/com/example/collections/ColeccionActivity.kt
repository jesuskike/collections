package com.example.collections

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.collections.BBDD.Chapas
import com.example.collections.BBDD.DataBase
import kotlinx.android.synthetic.main.activity_coleccion.*
import kotlinx.android.synthetic.main.lista_chapas.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File


class ColeccionActivity : AppCompatActivity() {

    // definimos la variable del nombre de la colección
    var nombreColeccion= ""

    // creacion de la base de datos
    lateinit var db: DataBase

    // codigo de respuesta del intent
    val REQUEST_CODE = 1

    // listado de chapas de la cava corresponidente
    private var listaChapas: ArrayList<Chapas> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coleccion)

        // instanciamos la BBDD
        db = DataBase.getInstance(applicationContext)!!

        // recogemos el nombre de la colección del intent
        nombreColeccion = intent.getStringExtra("nombreColeccion")

        // insertamos el nombre de la colección en el label
        tv_nombreColeccion.setText(nombreColeccion)

        // cargamos datos
        addItems()
        configView()
    }

    // sobreescribimos método para cerrar la activity en el back
    override fun onBackPressed() {
        finish()
    }

    // botón back pulsado, volvemos cerramos la activity
    fun rollback(view: View) {
        finish()
    }

    // llamamos a la actividad de añadir chapas
    fun addchapas(view: View) {
        // activamos la actividad de añadir chapas
        startActivityForResult(
            Intent(this, AnyadirChapas::class.java).apply
            { putExtra("nombreCava", nombreColeccion) }, REQUEST_CODE)
    }

    // recogemos los nombres de la lista del Recycle View
    fun mostrarChapa(view: View) {

        // recogemos el texto asignado al botón que será el nombre de la colección
        //val chapa = view.tv_nombreChapa.text
        val chapa = view.mostrarChapa.getTag().toString()

        // definimos el intent
        val intent = Intent(this, MostrarChapasActivity::class.java)

        // enviamos el nombre de la colección y la chapa para la búsqueda en la BBDD
        intent.putExtra("chapa", chapa.toString())
        intent.putExtra("cava", nombreColeccion)
        startActivity(intent)
    }

    // borramos la chapa seleccionada
    fun borrarChapa(view: View){

        // verificamos que queremos borrar la chapa mostramos dialogo de confirmación
        val alertbox = AlertDialog.Builder(this)

        // asignamos la cadena a mostrar
        alertbox.setMessage(R.string.confirmacionChapa)

        // pulsamos sobre si
        alertbox.setPositiveButton( R.string.yes,
            DialogInterface.OnClickListener { arg0, arg1 ->

                GlobalScope.launch {
                    // ha pulsado que si y borramos la chapa, recuperamos el indice
                    val idChapa = view.bt_borrarChapa.getContentDescription().toString()

                    // recuperamos la chapa
                    val chapa = db.cavaDao().infoChapa(idChapa.toInt(), nombreColeccion)

                    // borramos la foto del dispositivo
                    val fichero = File(chapa.foto_asociada)
                    fichero.delete()

                    // borramos la chapa de la BBDD
                    db.cavaDao().borrarChapa(chapa)

                    // borramos la chapa de la lista
                    listaChapas.remove(chapa)
                }

                Handler().postDelayed(Runnable {
                    // recargamos la vista
                    configView()
                }, 1)
            })

        // pulsamos sobre NO
        alertbox.setNegativeButton( R.string.no,
            DialogInterface.OnClickListener { arg0, arg1 ->

                //ha pulsado no y cancelamos el borrrado
                arg0.cancel()
            })

        //mostramos el alertbox
        alertbox.show()
    }

    // recogemos las chapas de la colección
    fun addItems() {
        GlobalScope.launch {
            // borramos la lista
            listaChapas.clear()

            // recuperamos el id de la cava
            val items = db.cavaDao().loadChapas(nombreColeccion)

            // recorremos la lista obtenida para insertarla en la Recicle View
            for (chapas in items) {
                listaChapas.add(chapas)
            }
        }
    }

    // configuración de la vista de la Recicle View
    private fun configView() {
        rv_chapas.adapter = AdapterChapas(listaChapas, this)
        rv_chapas.layoutManager = LinearLayoutManager( this)
    }

    // recargamos la actividad después de volver de la anterior
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // comprobamos la recepción de los datos
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            // recargamos la lista
            addItems()
            configView()
        }
    }

}
